# [Alfresco](https://gitlab.com/chainrecord/scripts/-/raw/master/docker-compose/alfresco.sh) 

## How to use
```sh
$ mkdir <directory_name>
$ cd <directory_name>
$ curl -sSL https://gitlab.com/chainrecord/scripts/-/raw/master/docker-compose/alfresco.sh | bash -s -- up -u admin -p admin -d backend.<your_base_domain> -cr http-default
```

## Available parameters
* `-u admin` - basic auth user for protected endpoints
* `-p 1122334455` - basic auth user password for protected endpoints
* `-cr http-default` - Traefik TLS certificate resolver name (default: `default`) 
* `-d backend.test.domain.app` - Host rule for Traefik 
* `-mem-limit 8` - Memory limit for all deployed containers (default: `8`)
* `-network public-net` - External docker network name for Traefik (default: `front-tier`)
* `-data-dir` - Directory where data and logs of containers will be mounted (default: `./data`)