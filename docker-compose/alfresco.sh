#!/bin/bash

DATA_DIR=./data

function printHelp() {
  echo "Usage: "
  echo "  alfresco.sh <Mode> [Flags]"
  echo "    <Mode>"
  echo "      - 'up' - bring up Alfresco"
  echo "      - 'down' - shut down Alfresco"
  echo "      - 'create-template' - create alfresco deployment template"
  echo
  echo "    Flags:"
  echo "    -d <domain> -  specify full domain name for alfresco ex. 'alfresco.eu4d.i4u.app' (MANDATORY)"
  echo "    -u <username> -  specify username for protected endpoints (MANDATORY)"
  echo "    -p <password> -  specify password for protected endpoints (MANDATORY)"
  echo "    -data-dir <path to data directory> - specify a path where the data of Alfresco, Solr and other services would be mounted (DEFAULT: ./data)"
  echo "    -cr <traefik cert resolver> - specify a name of traefik ACME certificate resolver, if no resolver is specified - then 'default' is used (OPTIONAL)"
  echo "    -mem-limit <memory limit> - specify how much memory to allocate for deployed instances, by default 8GB is set (OPTIONAL)"
  echo "    -network <traefik network> - specify a traefik network that will be used for exposing service to external traffic (OPTIONAL)"

  echo "  network.sh -h (print this message)"
  echo
  echo " Possible Mode and flags"
  echo "  alfresco.sh up -u -p -d -data-dir -cr -mem-limit -network"
  echo "  alfresco.sh down"
  echo
  echo " Taking all defaults:"
  echo "        alfresco.sh up"
  echo
  echo " Examples:"
  echo "  alfresco.sh up -u admin -p 123456 -d alfresco.eu4d.i4u.app"
  echo "  alfresco.sh up -u admin -p 123456 -d alfresco.eu4d.i4u.app -cr http-default"
  echo "  alfresco.sh up -u admin -p 123456 -d alfresco.eu4d.i4u.app -cr http-default -mem-limit 16"
  echo "  alfresco.sh up -u admin -p 123456 -d alfresco.eu4d.i4u.app -cr http-default -mem-limit 16 -network front-tier"
  echo "  alfresco.sh up -u admin -p 123456 -d alfresco.eu4d.i4u.app -cr http-default -mem-limit 16 -network front-tier -data-dir /var/opt/docker/data"
  echo "  alfresco.sh down"
}

function createAlfrescoTemplates() {
    mkdir -p config -p ${DATA_DIR}/data/solr-data ${DATA_DIR}/logs/alfresco ${DATA_DIR}/logs/postgres ${DATA_DIR}/logs/share
    mkdir -p share/web-extension share/modules/amps share/modules/jars search alfresco/modules/amps alfresco/modules/jars
    sudo chown -R 33007 ${DATA_DIR}/data/solr-data
    sudo chown -R 999 ${DATA_DIR}/logs

    # Generate share Tomcat config which disables CSRF
    genShare

    # Generate Dockerfiles for custom docker images
    genSearchDockerfile
    genAlfrescoDockerfile
    genShareDockerfile

    # Generate Nginx configuration files
    genNginx

    # Generate docker-compose file for Alfresco
    genAlfrescoEnv
    genAlfrescoDeployment
}

function deployAlfresco() {
    docker-compose up -d
}

function genSearchDockerfile {
    cat > search/Dockerfile <<-END
ARG SEARCH_TAG
FROM alfresco/alfresco-search-services:\${SEARCH_TAG}

# COMMON
ARG ALFRESCO_HOSTNAME
ARG SOLR_HOSTNAME
ENV ALFRESCO_HOSTNAME \$ALFRESCO_HOSTNAME
ENV SOLR_HOSTNAME \$SOLR_HOSTNAME

# Configure Alfresco Service Name
RUN sed -i '/^bash.*/i sed -i "'"s/alfresco.host=localhost/alfresco.host=\${ALFRESCO_HOSTNAME}/g"'" \${DIST_DIR}/solrhome/templates/rerank/conf/solrcore.properties\n' \
    \${DIST_DIR}/solr/bin/search_config_setup.sh && \
    sed -i '/^bash.*/i sed -i "'"s/solr.host=localhost/solr.host=\${SOLR_HOSTNAME}/g"'" \${DIST_DIR}/solrhome/conf/shared.properties\n' \
    \${DIST_DIR}/solr/bin/search_config_setup.sh

# Cross Locale
ARG CROSS_LOCALE
ENV CROSS_LOCALE \$CROSS_LOCALE

# Enable Cross Locale SOLR Configuration
RUN if [ "\$CROSS_LOCALE" == "true" ] ; then \
    sed -i '/^bash.*/i sed -i "'"/alfresco.cross.locale.datatype/s/^#//g"'" \$DIST_DIR/solrhome/conf/shared.properties\n' \
    \${DIST_DIR}/solr/bin/search_config_setup.sh; \
fi

# COMMS
ARG ALFRESCO_COMMS
ENV ALFRESCO_COMMS \$ALFRESCO_COMMS

# Configure SOLR cores to run in HTTPs mode from template
RUN if [ "\$ALFRESCO_COMMS" == "https" ] ; then \
    sed -i '/^bash.*/i sed -i "'"s/alfresco.secureComms=none/alfresco.secureComms=https/g"'" \${DIST_DIR}/solrhome/templates/rerank/conf/solrcore.properties\n' \
    \${DIST_DIR}/solr/bin/search_config_setup.sh; \
else \
    sed -i '/^bash.*/i sed -i "'"s/alfresco.secureComms=https/alfresco.secureComms=none/g"'" \${DIST_DIR}/solrhome/templates/rerank/conf/solrcore.properties\n' \
    \${DIST_DIR}/solr/bin/search_config_setup.sh; \
fi
END
}

function genShare {
    cat > share/web-extension/share-config-custom-dev.xml <<-END
<alfresco-config>
    <config evaluator="string-compare" condition="Users" replace="true">
      <users>
         <!-- minimum length for username and password -->
         <username-min-length>2</username-min-length>
         <password-min-length>3</password-min-length>
         <show-authorization-status>false</show-authorization-status>
      </users>
      <!-- This enables/disables the Add External Users Panel on the Add Users page. -->
      <enable-external-users-panel>false</enable-external-users-panel>
    </config>
    <config evaluator="string-compare" condition="CSRFPolicy" replace="true">
      <filter/>
    </config>
</alfresco-config>
END
}

function genShareDockerfile {
    cat > share/Dockerfile <<-END
ARG SHARE_TAG
FROM alfresco/alfresco-share:\${SHARE_TAG}

ARG TOMCAT_DIR=/usr/local/tomcat

USER root



# Install modules and addons
RUN mkdir -p \$TOMCAT_DIR/amps
COPY modules/amps \$TOMCAT_DIR/amps
COPY modules/jars \$TOMCAT_DIR/webapps/share/WEB-INF/lib

RUN java -jar \$TOMCAT_DIR/alfresco-mmt/alfresco-mmt*.jar install \
    \$TOMCAT_DIR/amps \$TOMCAT_DIR/webapps/share -directory -nobackup -force

# Fix for https://github.com/Alfresco/acs-community-packaging/issues/367 in Share 6.2.0
COPY web-extension/share-config-custom-dev.xml \$TOMCAT_DIR/shared/classes/alfresco/web-extension/
END
}

function genNginx {
    cat > config/nginx.conf <<-END
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    server {
        listen *:80;

        client_max_body_size 0;

        set  \$allowOriginSite *;
        proxy_pass_request_headers on;
        proxy_pass_header Set-Cookie;



        # External settings, do not remove
        #ENV_ACCESS_LOG

        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;
        proxy_buffering off;
        proxy_set_header Host            \$host:\$server_port;
        proxy_set_header X-Real-IP       \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_pass_header Set-Cookie;

        # Protect access to SOLR APIs
        location ~ ^(/.*/service/api/solr/.*)\$ {return 403;}
        location ~ ^(/.*/s/api/solr/.*)\$ {return 403;}
        location ~ ^(/.*/wcservice/api/solr/.*)\$ {return 403;}
        location ~ ^(/.*/wcs/api/solr/.*)\$ {return 403;}

        location ~ ^(/.*/proxy/alfresco/api/solr/.*)\$ {return 403 ;}
        location ~ ^(/.*/-default-/proxy/alfresco/api/.*)\$ {return 403;}

        # Alfresco Repository
        location /alfresco/ {
            proxy_pass http://alfresco:8080;
        }

        # REST API (Swagger)
        location /api-explorer/ {
            proxy_pass http://alfresco:8080;
        }

        # SOLR Web Console (Master)
        location /solr/ {

            proxy_pass http://solr6:8983;

            # Basic authentication
            auth_basic "Solr web console";
            auth_basic_user_file /etc/nginx/conf.d/nginx.htpasswd;

        }

        # Alfresco Share Web App
        location /share/ {
            proxy_pass http://share:8080;
        }

        # Alfresco Content App
        location / {
            proxy_pass http://content-app:8080;
        }

    }

}
END
    htpasswd -nb $USR $PSW >> config/nginx.htpasswd
}

function genAlfrescoEnv {
    cat > .env <<-END
ALFRESCO_COMMS=none
ALFRESCO_CE_TAG=6.2.0-ga
SEARCH_CE_TAG=1.4.1
SHARE_TAG=6.2.0
ACA_TAG=master-latest
POSTGRES_TAG=11.4
API_EXPLORER_TAG=6.2.0
TRANSFORM_ENGINE_TAG=2.1.0
ACTIVEMQ_TAG=5.15.8
DOMAIN=${DOMAIN}
CERT_RESOLVER=${CERT_RESOLVER}
END

}

function genAlfrescoDockerfile {
    cat > alfresco/Dockerfile <<-END
ARG ALFRESCO_TAG
FROM alfresco/alfresco-content-repository-community:\${ALFRESCO_TAG}

ARG TOMCAT_DIR=/usr/local/tomcat

USER root

# Install modules and addons
RUN mkdir -p \$TOMCAT_DIR/amps
COPY modules/amps \$TOMCAT_DIR/amps
COPY modules/jars \$TOMCAT_DIR/webapps/alfresco/WEB-INF/lib

RUN java -jar \$TOMCAT_DIR/alfresco-mmt/alfresco-mmt*.jar install \
    \$TOMCAT_DIR/amps \$TOMCAT_DIR/webapps/alfresco -directory -nobackup -force

# Install api-explorer webapp for REST API
ARG API_EXPLORER_TAG
ENV API_EXPLORER_TAG \$API_EXPLORER_TAG
RUN yum -y update && \
 yum -y install wget && \
 yum clean all && \
 set -x \
    && wget https://artifacts.alfresco.com/nexus/service/local/repositories/releases/content/org/alfresco/api-explorer/\${API_EXPLORER_TAG}/api-explorer-\${API_EXPLORER_TAG}.war \
    -O /usr/local/tomcat/webapps/api-explorer.war

# DATABASE
ARG DB
ENV DB \$DB

# Install mysql JDBC driver
RUN if [ "\$DB" == "mariadb" ] ; then \
    set -x \
        && yum install -y wget \
        && yum clean all \
        && wget -P /tmp/ http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.13.tar.gz \
        && tar xvf /tmp/mysql-connector-java-8.0.13.tar.gz -C /tmp/ \
        && cp /tmp/mysql-connector-java-8.0.13/mysql-connector-java-8.0.13.jar \$TOMCAT_DIR/lib/ \
        && rm -rf /tmp/mysql-connector-java-8.0.13.tar.gz /tmp/mysql-connector-java-8.0.13; \
fi

END
}

function genAlfrescoDeployment {
    cat > docker-compose.yml <<-END
# Using version 2 as 3 does not support resource constraint options (cpu_*, mem_* limits) for non swarm mode in Compose
version: "2"

networks:
    ${NETWORK}:
        external: true
    back-tier:

services:
    alfresco:
        restart: always
        #image: alfresco/alfresco-content-repository:6.2.1.2
        build:
          context: ./alfresco
          args:
            ALFRESCO_TAG: \${ALFRESCO_CE_TAG}
            DB: postgres
            API_EXPLORER_TAG: \${API_EXPLORER_TAG}
        mem_limit: ${ALFRESCO_MEM_LIMIT}
        depends_on:
            - postgres
        environment:
            JAVA_OPTS : '
                -Ddb.driver=org.postgresql.Driver
                -Ddb.username=alfresco
                -Ddb.password=alfresco
                -Ddb.driver=org.postgresql.Driver
                -Ddb.url=jdbc:postgresql://postgres:5432/alfresco
                -Dsolr.host=solr6
                -Dsolr.port=8983
                -Dsolr.secureComms=none
                -Dsolr.baseUrl=/solr
                -Dindex.subsystem.name=solr6
                -Dalfresco.host=localhost
                -Dalfresco.port=80

                -Dshare.host=localhost
                -Dshare.port=80

                -Daos.baseUrlOverwrite=http://localhost/alfresco/aos
                -Dmessaging.broker.url="failover:(nio://activemq:61616)?timeout=3000&jms.useCompression=true"
                -Ddeployment.method=DOCKER_COMPOSE
                -Dcsrf.filter.enabled=false
                -Dlocal.transform.service.enabled=true
                -DlocalTransform.pdfrenderer.url=http://alfresco-pdf-renderer:8090/
                -DlocalTransform.imagemagick.url=http://imagemagick:8090/
                -DlocalTransform.libreoffice.url=http://libreoffice:8090/
                -DlocalTransform.tika.url=http://tika:8090/
                -DlocalTransform.misc.url=http://transform-misc:8090/
                -Dlegacy.transform.service.enabled=true
                -Dalfresco-pdf-renderer.url=http://alfresco-pdf-renderer:8090/
                -Djodconverter.url=http://libreoffice:8090/
                -Dimg.url=http://imagemagick:8090/
                -Dtika.url=http://tika:8090/
                -Dtransform.misc.url=http://transform-misc:8090/
                -Dcsrf.filter.enabled=false
                -Dalfresco.restApi.basicAuthScheme=true
                -Dauthentication.protection.enabled=false
                -XX:+UseG1GC -XX:+UseStringDeduplication
                -Xms1856m -Xmx1856m
            '
        volumes:
            - ${DATA_DIR}/data/alf_data:/usr/local/tomcat/alf_data
            - ${DATA_DIR}/logs/alfresco:/usr/local/tomcat/logs
        networks:
            - back-tier


    alfresco-pdf-renderer:
        restart: always
        image: alfresco/alfresco-pdf-renderer:\${TRANSFORM_ENGINE_TAG}
        mem_limit: ${ALFRESCOPDFRENDERER_MEM_LIMIT}
        environment:
            JAVA_OPTS: " -Xms256m -Xmx512m"
        networks:
            - back-tier

    imagemagick:
        restart: always
        image: alfresco/alfresco-imagemagick:\${TRANSFORM_ENGINE_TAG}
        mem_limit: ${IMAGEMAGICK_MEM_LIMIT}
        environment:
            JAVA_OPTS: " -Xms256m -Xmx512m"
        networks:
            - back-tier

    libreoffice:
        restart: always
        image: alfresco/alfresco-libreoffice:\${TRANSFORM_ENGINE_TAG}
        mem_limit: ${LIBREOFFICE_MEM_LIMIT}
        environment:
            JAVA_OPTS: " -Xms256m -Xmx512m"
        networks:
            - back-tier

    tika:
        restart: always
        image: alfresco/alfresco-tika:\${TRANSFORM_ENGINE_TAG}
        mem_limit: ${TIKA_MEM_LIMIT}
        environment:
            JAVA_OPTS: " -Xms256m -Xmx512m"
        networks:
            - back-tier

    transform-misc:
        restart: always
        image: alfresco/alfresco-transform-misc:\${TRANSFORM_ENGINE_TAG}
        mem_limit: ${TRANSFORMMISC_MEM_LIMIT}
        environment:
            JAVA_OPTS: " -Xms256m -Xmx512m"
        networks:
            - back-tier

    share:
        restart: always
        #image: alfresco/alfresco-share:6.2.2
        build:
          context: ./share
          args:
            SHARE_TAG: \${SHARE_TAG}
        mem_limit: ${SHARE_MEM_LIMIT}
        environment:
            REPO_HOST: "alfresco"
            REPO_PORT: "8080"
            JAVA_OPTS: "
                -Xms464m -Xmx464m
                -Dalfresco.context=alfresco
                -Dalfresco.protocol=http
                "
        volumes:
            - ${DATA_DIR}/logs/share:/usr/local/tomcat/logs
            - ./share/web-extension/share-config-custom-dev.xml:/usr/local/tomcat/shared/classes/alfresco/web-extension/share-config-custom-dev.xml
        networks:
            - back-tier


    postgres:
        restart: always
        image: postgres:\${POSTGRES_TAG}
        mem_limit: ${POSTGRES_MEM_LIMIT}
        environment:
            - POSTGRES_PASSWORD=alfresco
            - POSTGRES_USER=alfresco
            - POSTGRES_DB=alfresco
        command: "
            postgres
              -c max_connections=200
              -c logging_collector=on
              -c log_min_messages=LOG
              -c log_directory=/var/log/postgresql"
        volumes:
            - ${DATA_DIR}/data/postgres:/var/lib/postgresql/data
            - ${DATA_DIR}/logs/postgres:/var/log/postgresql
        networks:
            - back-tier


    solr6:
        restart: always
        #image: alfresco/alfresco-search-services:1.4.2
        build:
          context: ./search
          args:
            SEARCH_TAG: \$SEARCH_CE_TAG
            SOLR_HOSTNAME: solr6
            ALFRESCO_HOSTNAME: alfresco
            ALFRESCO_COMMS: none
            CROSS_LOCALE: "true"
        mem_limit: ${SOLR6_MEM_LIMIT}
        environment:
            #Solr needs to know how to register itself with Alfresco
            SOLR_ALFRESCO_HOST: "alfresco"
            SOLR_ALFRESCO_PORT: "8080"
            #Alfresco needs to know how to call solr
            SOLR_SOLR_HOST: "solr6"
            SOLR_SOLR_PORT: "8983"
            #Create the default alfresco and archive cores
            SOLR_CREATE_ALFRESCO_DEFAULTS: "alfresco,archive"
            SOLR_JAVA_MEM: "-Xms928m -Xmx928m"
            SOLR_OPTS: "
                -XX:NewSize=336m
                -XX:MaxNewSize=336m
            "
        volumes:
            - ${DATA_DIR}/data/solr-data:/opt/alfresco-search-services/data
        networks:
            - back-tier

    activemq:
        restart: always
        image: alfresco/alfresco-activemq:\${ACTIVEMQ_TAG}
        mem_limit: ${ACTIVEMQ_MEM_LIMIT}
        networks:
            - back-tier

    content-app:
        restart: always
        image: alfresco/alfresco-content-app:\${ACA_TAG}
        mem_limit: ${CONTENTAPP_MEM_LIMIT}
        depends_on:
            - alfresco
            - share
        networks:
            - back-tier

    # HTTP proxy to provide HTTP Default port access to services
    # SOLR API and SOLR Web Console are protected to avoid unauthenticated access
    proxy:
        restart: always
        image: nginx:stable-alpine
        mem_limit: ${PROXY_MEM_LIMIT}
        depends_on:
            - alfresco
            - solr6
            - share
            - content-app
        volumes:
            - ./config/nginx.conf:/etc/nginx/nginx.conf
            - ./config/nginx.htpasswd:/etc/nginx/conf.d/nginx.htpasswd
        labels:
            traefik.enable: true
            traefik.http.routers.alfresco-proxy-http.rule: "Host(\`${DOMAIN?err}\`)"
            traefik.http.routers.alfresco-proxy-http.entrypoints: http
            traefik.http.routers.alfresco-proxy-http.middlewares: default-https
            traefik.http.routers.alfresco-proxy-https.rule: "Host(\`${DOMAIN?err}\`)"
            traefik.http.routers.alfresco-proxy-https.entrypoints: https
            traefik.http.routers.alfresco-proxy-https.tls.certResolver: ${CERT_RESOLVER?err}
            traefik.http.services.alfresco-proxy.loadbalancer.server.port: 80
        networks:
            - back-tier
            - ${NETWORK}
END
}

function setMemLimitVariables {
    MB=$(expr $MEM_LIMIT \* 1024)

    PROXY_MEM_LIMIT=$(expr $MB / 64)m
    CONTENTAPP_MEM_LIMIT=$(expr $MB / 32)m
    ACTIVEMQ_MEM_LIMIT=$(expr $MB / 8)m
    SOLR6_MEM_LIMIT=$(expr $MB / 8)m
    POSTGRES_MEM_LIMIT=$(expr $MB / 16)m
    SHARE_MEM_LIMIT=$(expr $MB / 16)m
    TRANSFORMMISC_MEM_LIMIT=$(expr $MB / 16)m
    TIKA_MEM_LIMIT=$(expr $MB / 16)m
    LIBREOFFICE_MEM_LIMIT=$(expr $MB / 16)m
    IMAGEMAGICK_MEM_LIMIT=$(expr $MB / 16)m
    ALFRESCOPDFRENDERER_MEM_LIMIT=$(expr $MB / 16)m
    ALFRESCO_MEM_LIMIT=$(expr $MB / 3)m
}

## Parse mode
if [[ $# -lt 1 ]] ; then
  printHelp
  exit 0
else
  MODE=$1
  shift
fi


# parse flags
while [[ $# -ge 1 ]] ; do
  key="$1"
  case $key in
  -h )
    printHelp
    exit 0
    ;;
  -u )
    USR="$2"
    shift
    ;;
  -p )
    PSW="$2"
    shift
    ;;
  -d )
    DOMAIN="$2"
    shift
    ;;
  -cr )
    CERT_RESOLVER="$2"
    shift
    ;;
  -mem-limit )
    MEM_LIMIT="$2"
    shift
    ;;
  -network )
    NETWORK="$2"
    shift
    ;;
  -data-dir )
    DATA_DIR="$2"
    shift
    ;;
  * )
    echo
    echo "Unknown flag: $key"
    echo
    printHelp
    exit 1
    ;;
  esac
  shift
done


if [ "$MODE" == "up" ]; then
    if [ -z "$USR" ] || [ -z "$PSW" ] || [ -z "$DOMAIN" ]; then
        echo "ERROR: mandatory flags are missing ('-u' or '-p' or '-d')"
        printHelp
        exit 1
    fi

    if [ -z "$NETWORK" ]; then
        NETWORK=front-tier
    fi

    if [ -z "$MEM_LIMIT" ]; then
        MEM_LIMIT=8
    fi
    setMemLimitVariables

    if [ -z "$CERT_RESOLVER" ]; then
        CERT_RESOLVER=default
    fi

    createAlfrescoTemplates $USR $PSW
    docker-compose up -d
elif [ "$MODE" == "down" ]; then
    COMPOSE_FILE=$(cat ./docker-compose.yml)
    if [ -z "$COMPOSE_FILE" ]; then
        echo "ERROR: alfresco has not been deployed (missing 'docker-compose.yml' file)"
        exit 1
    fi
    docker-compose down
elif [ "$MODE" == "create-template" ]; then
    if [ -z "$USR" ] || [ -z "$PSW" ] || [ -z "$DOMAIN" ]; then
        echo "ERROR: mandatory flags are missing ('-u' or '-p' or '-d')"
        printHelp
        exit 1
    fi

    if [ -z "$NETWORK" ]; then
        NETWORK=front-tier
    fi

    if [ -z "$MEM_LIMIT" ]; then
        MEM_LIMIT=8
    fi
    setMemLimitVariables

    if [ -z "$CERT_RESOLVER" ]; then
        CERT_RESOLVER=default
    fi

    createAlfrescoTemplates $USR $PSW

else
  printHelp
  exit 1
fi

echo "======================================"
echo "=============> SUCCESS <=============="
echo "======================================"
